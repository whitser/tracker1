<script>
// JavaScript Document
// Set the date we're counting down to
var countDownDate = <?php echo $start; ?>;
//var countDownDate = new Date("Jul 24, 2021 11:45:40").getTime();
											   
// Update the count down every 1 second
var x = setInterval(function() {
																
  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = now-(countDownDate*1000);
    
  // Time calculations for days, hours, minutes and seconds
  // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor(distance / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  
  //document.getElementById("demo").innerHTML = "Dist: " + distance + "Now: "+now+"Cdd: "+countDownDate;
  
  document.getElementById("demo").innerHTML = "This session has lasted " + hours + "h "
  + minutes + "m " + seconds + "s so far.";
  
}, 1000);
</script>