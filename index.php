<?php
// page to time things
include('db.php');
if (isset($_POST['submitted'])) {
  if ($_POST['submitted']==1 && strlen($_POST['name'])>0) {
    $start = time();
    $name=$_POST['name'];
    $name=preg_replace('/\s/','/\_/',$name);
    $name=preg_replace('/\W/','',$name);
    $name=strtolower($name);
    $dbname=mysqli_real_escape_string($link,$name);
    $query="insert into tracker.instance_log (name,actiontype) values ('$name','start')";
	if (!mysqli_query($link,$query)) {
	   echo "error with query $query";
	}
	$query="select name, sum(duration) totdur from durations group by name having name='$name'";
	$result=mysqli_query($link,$query);
	if (mysqli_num_rows($result)==1) {
	  while ($row = mysqli_fetch_array($result)) {
		$tothours=floor($row['totdur']/3600);
		$totmins=floor(($row['totdur']-$tothours)/60);
		$totsecs=floor($row['totdur'] % 60);
		$top = "<div>Exisiting project ".htmlspecialchars($name)." has been resumed.</div>";
		$existing = "<div>The previous time used on this project was $tothours h $totmins m $totsecs s.</div>";
	  }
	} else {
	  $top = "<div>Project saved with name ".htmlspecialchars($name)."</div>";
	  $existing="<div>This is a new project</div>";
	}
    //echo "<div>Start time is: ".$starttime."</div>";
    echo $top;
	echo "<div>Your start time is ".date('H:i:s',$start)." on ".date('D, d-m-Y',$start).".</div>";
   echo "<form name=\"Stop\" method=\"post\"><input type=\"hidden\" name=\"name\" value=\"$name\" />";
	echo "<input type=\"hidden\" name=\"start\" value=\"$start\" />";
	echo "<br /><p>Click here to finish task: <input type=\"submit\" name=\"Stop\" value=\"Stop\" /></p>";
  include('counter.js');
	
	echo "<p id=\"demo\"></p>";
	echo $existing;
	die();
  } else {
    echo "<div>Either you didn't enter your name or something untoward has happened! Please try again...</div>";
  }
}
if (isset($_POST['start'])) {
    $end = time();
    $start=$_POST['start'];
    $name=$_POST['name'];
    
    $dbname=mysqli_real_escape_string($link,$name);
    $query="insert into tracker.instance_log (name,actiontype) values ('$name','end')";
	mysqli_query($link,$query);
	
    $duration = $end - $start;
    //echo "<div>End time is: ".$end."</div>";
    echo "<div>Hello ".htmlspecialchars($name).", your endtime is ".date('d-m-y H:i:s',$end).". Your start time was ".date('d-m-y H:i:s',$start)."</div>";
    echo "<div>The time taken for the task was ".$duration." seconds</div>";
    $query="insert into tracker.durations (name,duration) values ('$name',$duration)";
	mysqli_query($link,$query);    
    echo "<br /><p>Click <a href=\"index.php\">here</a> to start a new project and view list of existing projects.</p>";
    die();
}
echo '<html>
<body>
<form name="starttime" method="post" target="index.php">
  <div>Hello, please enter the project name below and click start.</div>
  <div>Note that project names are not case sensitive and should only contain alphanumeric characters or spaces.</div>
  <div>If you wish to continue an existing project, existing project names are below.</div>
  <div><input type="text" name="name" /></div>
  <div><input type="submit" name="Start" value="Start"/><input type="hidden" name="submitted" value="1" /></div>
</form>';

    $query="select name, sum(duration) totdur from durations group by name";
	$result=mysqli_query($link,$query);
	if (mysqli_num_rows($result)>0) { 
	  echo "<table><tr><th>Project Name</th><th>Hours</th><th>Minutes</th><th>Seconds</th></tr>";
	  while ($row = mysqli_fetch_array($result)) {
		echo "<tr><td>".$row['name']."</td>";
		$tothours=floor($row['totdur']/3600);
		$totmins=floor(($row['totdur']-$tothours)/60);
		$totsecs=floor($row['totdur'] % 60);  
		echo "<td>$tothours</td>";
		echo "<td>$totmins</td>";
		echo "<td>$totsecs</td>";
		echo "</tr>";
	  }
	  echo "</table>";
	} else {
	  echo "<div>No projects have been saved on the system yet.</div>";
  }  
  echo "</body></html>";