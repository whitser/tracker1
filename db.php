<?php
$username='root';
$hostname='localhost';
$password='';

$link=mysqli_connect($hostname,$username,$password);
if (!$link) {
  echo "Unable to connect to server";
  die();
} 

if (!mysqli_set_charset($link,'utf8')) {
   echo " Unable to set up encoding.";
   die();
}

$dbname='tracker';
if (!mysqli_select_db($link,$dbname)) {
  echo " Unable to open database: ".mysqli_error($link);
  die();
}

?>
