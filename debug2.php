<html>
<body>
<form name="starttime" method="post">
  <div>Hello, please enter the project name below and click start.</div>
  <div>Note that project names are not case sensitive and should only contain alphanumeric characters or spaces.</div>
  <div>If you wish to continue an existing project, existing project names are below.</div>
  <div><input type="text" name="name" /></div>
  <div><input type="submit" name="Start" value="Start"/><input type="hidden" name="submitted" value="1" /></div>
</form>
<?php
    echo "HERE";
    $query="select name, sum(duration) totdur from durations group by name";
	$result=mysqli_query($link,$query);
	if (mysqli_num_rows($result)>0) { 
	  echo "<table><tr><th>Project Name</th><th>Hours</th><th>Minutes</th><th>Seconds</th></tr>";
	  while ($row = mysqli_fetch_array($result)) {
		echo "<tr><td>".$row['name']."</td>";
		$tothours=floor($row['totdur']/3600);
		$totmins=floor(($row['totdur']-$tothours)/60);
		$totsecs=floor($row['totdur'] % 60);  
		echo "<td>$tothours</td>";
		echo "<td>$totmins</td>";
		echo "<td>$totsecs</td>";
		echo "</tr>";
	  }
	  echo "</table>";
	} else {
	  echo "<div>No projects have been saved on the system yet.</div>";
  }
  
  echo "</body></html>";